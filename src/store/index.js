import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    id : ''
  },
  mutations: {
    userID: function(state, payload){
      state.id = payload;
      console.log(state.id);
      return state.id;
    }
  },
  actions: {
  },
  modules: {
  }
})

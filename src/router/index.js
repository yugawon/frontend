import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Panorama from '../views/Panorama.vue'
import List from '../views/List.vue'
import Login from '../views/Login.vue'
import Admin from '../views/Admin.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path:'/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/panorama/:dir_name',
    name: 'Panorama',
    component: Panorama,
    props: true
  },
  {
    path: '/list',
    name: 'List',
    component: List
  },
  {
    path:'/admin',
    name: 'Admin',
    component: Admin
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
